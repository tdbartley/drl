Experiment 1
 - Train on randomized episodes.
 - Test on fixed episode, for all skills. Record position history.

Experiment 2
 - Train on fixed episode.
 - Test on fixed episode, for all skills. Record position history.
