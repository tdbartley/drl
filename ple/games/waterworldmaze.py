from os import listdir
from os.path import isfile, join, dirname
import pygame as pg
import sys
import copy
import numpy as np
from .base.pygamewrapper import PyGameWrapper
from .utils.vec2d import vec2d
from pygame.constants import K_w, K_a, K_s, K_d


def process_state(state):
    state_list = []
    state_list.append(state["player_pos"][0])
    state_list.append(state["player_pos"][1])
    state_list.append(state["player_vel"][0])
    state_list.append(state["player_vel"][1])
    for creep_pos in state["creep_pos"]:
        state_list.append(creep_pos[0])
        state_list.append(creep_pos[1])
    for wall_pos in state["wall_pos"]:
        state_list.append(wall_pos[0])
        state_list.append(wall_pos[1])
    return np.array(state_list, dtype=np.float32)


class Wall(pg.sprite.Sprite):

    def __init__(self, x, y, width, height):
        pg.sprite.Sprite.__init__(self)
        image = pg.Surface([width, height])
        image.set_colorkey((0, 0, 0))
        pg.draw.rect(
            image,
            (10, 10, 10),
            [0, 0, width, height],
            0
        )
        self.image = image.convert()
        self.rect = self.image.get_rect()
        self.height = height
        self.width = width
        self.rect.x = x * width
        self.rect.y = y * height
        self.mask = pg.mask.from_surface(self.image)

    def draw(self, screen):
        screen.blit(self.image, self.rect)


class Player(pg.sprite.Sprite):

    def __init__(self,
                 radius,
                 color,
                 pos,
                 screen_width,
                 screen_height
    ):
        pg.sprite.Sprite.__init__(self)
        self.screen_width = screen_width
        self.screen_height = screen_height
        self.pos = vec2d((pos.x, pos.y))
        self.vel = vec2d((0.0, 0.0))
        image = pg.Surface([radius * 2, radius * 2])
        image.set_colorkey((0, 0, 0))
        pg.draw.circle(
            image,
            color,
            (radius, radius),
            radius,
            0
        )
        self.image = image.convert()
        self.rect = self.image.get_rect()
        self.rect.x = self.pos.x
        self.rect.y = self.pos.y
        self.radius = radius
        self.mask = pg.mask.from_surface(self.image)
    def draw(self, screen):
        screen.blit(self.image, self.rect)
    def set_color(self, color):
        image = pg.Surface([self.radius * 2, self.radius * 2])
        image.set_colorkey((0, 0, 0))
        pg.draw.circle(
            image,
            color,
            (self.radius, self.radius),
            self.radius,
            0
        )
        self.image = image.convert()
        self.rect = self.image.get_rect()
        self.rect.x = self.pos.x
        self.rect.y = self.pos.y
        self.mask = pg.mask.from_surface(self.image)


class Creep(pg.sprite.Sprite):

    def __init__(self,
                 color,
                 radius,
                 pos_init
    ):

        pg.sprite.Sprite.__init__(self)
        image = pg.Surface([radius * 2, radius * 2])
        image.set_colorkey((0, 0, 0))
        pg.draw.circle(
            image,
            color,
            (radius, radius),
            radius,
            0
        )
        self.image = image.convert()
        self.rect = self.image.get_rect()
        self.rect.x = pos_init[0]
        self.rect.y = pos_init[1]
        self.radius = radius

    def draw(self, screen):
        screen.blit(self.image, self.rect)


class Goal(pg.sprite.Sprite):

    def __init__(self,
                 color,
                 radius,
                 pos_init
    ):

        pg.sprite.Sprite.__init__(self)
        image = pg.Surface([radius * 2, radius * 2])
        image.set_colorkey((0, 0, 0))
        pg.draw.circle(
            image,
            color,
            (radius, radius),
            radius,
            0
        )
        self.image = image.convert()
        self.rect = self.image.get_rect()
        self.rect.x = pos_init[0]
        self.rect.y = pos_init[1]
        self.radius = radius

    def draw(self, screen):
        screen.blit(self.image, self.rect)


class WaterWorldMaze(PyGameWrapper):
    """
    Parameters
    ----------
    width : int
        Screen width.

    height : int
        Screen height, recommended to be same dimension as width.

    num_creeps : int (default: 3)
        The number of creeps on the screen at once.
    """

    def __init__(self,
                 tile_size=16,
                 grid_width=9,
                 grid_height=9,
                 num_creeps=1,
                 macro=False,
                 deterministic=True,
                 #data_dir="mazes/empty/"
                 data_dir="mazes/9x9_sorted/31/",
                 #data_dir="mazes/5x5/7/"
                 ):

        actions = {
            "up": K_w,
            "left": K_a,
            "right": K_d,
            "down": K_s
        }

        screen_width = tile_size * grid_width
        screen_height = tile_size * grid_height

        PyGameWrapper.__init__(self, screen_width, screen_height, actions=actions)
        self.screen_width = screen_width
        self.screen_height = screen_height
        self.tile_size = tile_size
        self.grid_width = grid_width
        self.grid_height = grid_height
        self.num_creeps = num_creeps
        self.macro = macro
        self.data_dir = data_dir
        self.bg_color = (255, 255, 255)
        self.creep_counts = {
            "GOOD": 0
        }
        self.rewards["tick"] = -0.1
        self.agent_speed = tile_size * 1.0
        self.agent_momo = 0.975
        self.agent_color = (60, 60, 200)
        self.agent_radius = int(tile_size / 4)
        self.creep_color = (60, 200, 60)
        self.creep_radius = int(tile_size / 4)
        self.goal_color = (100, 30, 30)
        self.goal_radius = int(tile_size / 4)
        self.wall_width = self.tile_size
        self.wall_height = self.tile_size
        self.dx = 0
        self.dy = 0
        self.player = None
        self.creeps = None
        self.walls = None
        self.maze = None
        self.goals = None
        self.deterministic = deterministic
        self.recent_goals = []
        self.lives = -1
        self.ticks = 0
        self.last_action = 4

    def _handle_player_events(self):
        self.last_action = 4
        self.dx = 0
        self.dy = 0
        for event in pg.event.get():
            if event.type == pg.QUIT:
                pg.quit()
                sys.exit()
            if event.type == pg.KEYDOWN:
                key = event.key
                if key == self.actions["left"]:
                    self.dx -= self.agent_speed
                    self.last_action = 1
                if key == self.actions["right"]:
                    self.dx += self.agent_speed
                    self.last_action = 2
                if key == self.actions["up"]:
                    self.dy -= self.agent_speed
                    self.last_action = 0
                if key == self.actions["down"]:
                    self.dy += self.agent_speed
                    self.last_action = 3

    def _update_player(self, dx, dy, dt):
        pos_old = vec2d((self.player.pos.x, self.player.pos.y))

        if self.macro:
            pos_old.x = pos_old.x // self.tile_size * self.tile_size + self.tile_size / 2 - self.player.radius
            pos_old.y = pos_old.y // self.tile_size * self.tile_size + self.tile_size / 2 - self.player.radius
            pos_new = vec2d((pos_old.x + self.dx, pos_old.y + self.dy))
            self.player.pos.x = pos_new.x
            self.player.pos.y = pos_new.y
            self.player.rect.x = pos_new.x
            self.player.rect.y = pos_new.y

            # handle wall collisions
            wall_hits = pg.sprite.spritecollide(self.player, self.walls, False)
            oob_x = (pos_new.x < 0.0) or (pos_new.x > self.player.screen_width - 2 * self.player.radius)
            oob_y = (pos_new.y < 0.0) or (pos_new.y > self.player.screen_height - 2 * self.player.radius)
            if len(wall_hits) or oob_x or oob_y:
                self.player.pos.x = pos_old.x
                self.player.pos.y = pos_old.y
                self.player.rect.x = pos_old.x
                self.player.rect.y = pos_old.y

        else:
            pos_new = vec2d((pos_old.x + self.player.vel.x * dt, pos_old.y + self.player.vel.y * dt))
            pos_update = vec2d((pos_old.x + self.player.vel.x * dt, pos_old.y + self.player.vel.y * dt))
            self.player.pos.x = pos_new.x
            self.player.pos.y = pos_new.y
            self.player.rect.x = pos_new.x
            self.player.rect.y = pos_new.y
            self.player.vel.x += dx
            self.player.vel.y += dy

            # out of bounds
            oob_x = (pos_new.x < 0.0) or (pos_new.x > self.player.screen_width - 2 * self.player.radius)
            oob_y = (pos_new.y < 0.0) or (pos_new.y > self.player.screen_height - 2 * self.player.radius)
            if oob_x:
                self.player.vel.x = -self.player.vel.x
            if oob_y:
                self.player.vel.y = -self.player.vel.y
            if oob_x or oob_y:
                pos_update = pos_old

            # handle wall collisions
            wall_hits = pg.sprite.spritecollide(self.player, self.walls, False, pg.sprite.collide_mask)
            c_pos = vec2d((pos_new.x, pos_new.y))
            c_pos.x += self.player.radius
            c_pos.y += self.player.radius
            for wall in wall_hits:
                w_pos = vec2d((wall.rect.x, wall.rect.y))
                w_pos.x += self.tile_size / 2
                w_pos.y += self.tile_size / 2
                x_diff = c_pos.x - w_pos.x
                y_diff = c_pos.y - w_pos.y
                # east
                if abs(x_diff) >= abs(y_diff) and x_diff < 0:
                    self.player.vel.x = -abs(self.player.vel.x)
                    pos_update = pos_old
                # west
                if abs(x_diff) >= abs(y_diff) and x_diff > 0:
                    self.player.vel.x = abs(self.player.vel.x)
                    pos_update = pos_old
                # south
                if abs(y_diff) >= abs(x_diff) and y_diff < 0:
                    self.player.vel.y = -abs(self.player.vel.y)
                    pos_update = pos_old
                # north
                if abs(y_diff) >= abs(x_diff) and y_diff > 0:
                    self.player.vel.y = abs(self.player.vel.y)
                    pos_update = pos_old

            self.player.pos.x = pos_update.x
            self.player.pos.y = pos_update.y
            self.player.rect.x = pos_update.x
            self.player.rect.y = pos_update.y
            self.player.vel.x *= self.agent_momo
            self.player.vel.y *= self.agent_momo

    def _add_creep(self):
        if self.deterministic:
            creep_init_x = 6
            creep_init_y = 4
        else:
            creep_init_x = np.random.randint(self.grid_width)
            creep_init_y = np.random.randint(self.grid_height)
        player_pos = self.player.pos
        while (self.maze[creep_init_x, creep_init_y] or
               (creep_init_x == int(player_pos.x // self.tile_size) and
                creep_init_y == int(player_pos.y // self.tile_size))
        ):
            creep_init_x = np.random.randint(self.grid_width)
            creep_init_y = np.random.randint(self.grid_height)
        pos = (self.tile_size * creep_init_x + self.tile_size / 2 - self.creep_radius,
             self.tile_size * creep_init_y + self.tile_size / 2 - self.creep_radius)
        creep = Creep(
            self.creep_color,
            self.creep_radius,
            pos
        )
        self.creeps.add(creep)
        self.creep_counts['GOOD'] += 1

    def getGameState(self):
        state = {
            "maze": self.maze,
            "score": self.score,
            "ticks": self.ticks,
            "recent_goals": self.recent_goals,
            "macro": self.macro,
            "is_game_over": self.game_over(),
            "player_pos": [self.player.pos.x, self.player.pos.y],
            "player_tile": [int(self.player.pos.x // self.tile_size),
                            int(self.player.pos.y // self.tile_size)],
            "player_vel": [self.player.vel.x, self.player.vel.y],
            "creep_pos": [],
            "wall_pos": []
        }
        for creep in self.creeps:
            state["creep_pos"].append([creep.rect.x, creep.rect.y])
        for wall in self.walls:
            state["wall_pos"].append([wall.rect.x, wall.rect.y])
        return state

    def getScore(self):
        return self.score

    def game_over(self):
        """
            Return bool if the game has 'finished'
        """
        return ((self.creep_counts['GOOD'] == 0) or (not self.macro and (self.ticks == 1000)))

    def _set_goals(self, goal_positions):
        if self.goals is None:
            self.goals = pg.sprite.Group()
        else:
            self.goals.empty()
        for goal_pos in goal_positions:
            goal = Goal(
                self.goal_color,
                self.goal_radius,
                goal_pos
            )
            self.goals.add(goal)

    def _set_agent_color(self, color):
        self.player.set_color(color)

    def init(self, state=None):
        """
            Starts/Resets the game to its inital state
        """
        if state is not None:
            self.creep_counts = {"GOOD": 0}
            self.maze = state["maze"]
            self.score = state["score"]
            self.ticks = state["ticks"]
            self.recent_goals = state["recent_goals"]
            self.macro = state["macro"]

            # player
            if self.macro:
                pos_x = state["player_tile"][0] * self.tile_size
                pos_y = state["player_tile"][1] * self.tile_size
            else:
                pos_x = state["player_pos"][0]
                pos_y = state["player_pos"][1]
            pos = vec2d((pos_x, pos_y))
            self.player = Player(
                self.agent_radius,
                self.agent_color,
                pos,
                self.width,
                self.height
            )

            # creep
            if self.creeps is None:
                self.creeps = pg.sprite.Group()
            else:
                self.creeps.empty()
            creep = Creep(self.creep_color, self.creep_radius, [state["creep_pos"][0][0], state["creep_pos"][0][1]])
            self.creeps.add(creep)
            self.creep_counts['GOOD'] += 1

            # walls
            if self.walls is None:
                self.walls = pygame.sprite.Group()
            else:
                self.walls.empty()
            for i in range(self.grid_width):
                for j in range(self.grid_height):
                    if self.maze[i, j]:
                        wall = Wall(i, j, self.wall_width, self.wall_height)
                        self.walls.add(wall)
        else:
            self.creep_counts = {"GOOD": 0}

            # load random maze
            this_path = dirname(__file__)
            maze_path = join(this_path, self.data_dir)
            files = [f for f in listdir(maze_path) if isfile(join(maze_path, f))]
            if self.deterministic:
                maze_id = 7
            else:
                maze_id = np.random.randint(len(files))
            self.maze = np.load(maze_path + files[maze_id])

            # walls
            if self.walls is None:
                self.walls = pg.sprite.Group()
            else:
                self.walls.empty()
            for i in range(self.grid_width):
                for j in range(self.grid_height):
                    if self.maze[i, j]:
                        wall = Wall(i, j, self.tile_size, self.tile_size)
                        self.walls.add(wall)
            self.walls.draw(self.screen)

            # player
            if self.deterministic:
                pos_x = 0
                pos_y = 0
            else:
                pos_x = np.random.randint(self.grid_width)
                pos_y = np.random.randint(self.grid_height)
            while self.maze[pos_x, pos_y]:
                pos_x = np.random.randint(self.grid_width)
                pos_y = np.random.randint(self.grid_height)
            pos = vec2d((self.tile_size * pos_x + self.tile_size / 2.0 - self.agent_radius,
                         self.tile_size * pos_y + self.tile_size / 2.0 - self.agent_radius))
            self.player = Player(
                self.agent_radius,
                self.agent_color,
                pos,
                self.width,
                self.height
            )

            # creeps
            if self.creeps is None:
                self.creeps = pg.sprite.Group()
            else:
                self.creeps.empty()
            for i in range(self.num_creeps):
                self._add_creep()

            self.score = 0
            self.ticks = 0
            self.recent_goals = []

    def step(self, dt):
        """
            Perform one step of game emulation.
        """
        dt /= 1000.0

        self.screen.fill(self.bg_color)
        self._handle_player_events()
        self._update_player(self.dx, self.dy, dt)

        # handle creep collisions
        creep_hits = pg.sprite.spritecollide(self.player, self.creeps, False)
        for _ in creep_hits:
            self.creep_counts["GOOD"] -= 1
            self.score += self.rewards["positive"]

        if self.goals is not None:
            self.goals.draw(self.screen)
        self.walls.draw(self.screen)
        self.creeps.draw(self.screen)
        self.player.draw(self.screen)

        self.score += self.rewards["tick"]
        self.recent_goals.append(self.last_action)
        self.ticks += 1

        # win
        if self.creep_counts["GOOD"] == 0:
            self.score += self.rewards["win"]

        # lose
        if not self.macro and (self.ticks == 1000):
            self.score += self.rewards["loss"]

if __name__ == "__main__":
    import numpy as np

    pg.init()
    game = WaterWorldMaze(width=256, height=256, num_creeps=10)
    game.screen = pg.display.set_mode(game.getScreenDims(), 0, 32)
    game.clock = pg.time.Clock()
    game.rng = np.random.RandomState(24)
    game.init()

    while True:
        dtime = game.clock.tick_busy_loop(30)
        game.step(dtime)
        pg.display.update()
