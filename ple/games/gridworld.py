from os import listdir
from os.path import isfile, join
import sys
import pygame
import numpy as np
from .base.pygamewrapper import PyGameWrapper
from .utils.vec2d import vec2d
from pygame.constants import K_w, K_a, K_s, K_d


def process_state(state):
    state_list = []
    state_list.append(state["player_tile"][0])
    state_list.append(state["player_tile"][1])
    for creep_pos in state["creep_pos"]:
        state_list.append(creep_pos[0])
        state_list.append(creep_pos[1])
    for wall_pos in state["wall_pos"]:
        state_list.append(wall_pos[0])
        state_list.append(wall_pos[1])
    return np.array(state_list, dtype=np.float32)


class Player(pygame.sprite.Sprite):

    def __init__(self, pos, dim):
        pygame.sprite.Sprite.__init__(self)
        self.image = pygame.Surface([dim.x, dim.y])
        self.image.fill((60, 60, 200))
        self.rect = self.image.get_rect()
        self.rect.x = pos.x * dim.x
        self.rect.y = pos.y * dim.y
        self.dim = dim

    def get_position(self):
        return vec2d((self.rect.x // self.dim.x, self.rect.y // self.dim.y))

    def set_position(self, pos):
        self.rect.x = pos.x * self.dim.x
        self.rect.y = pos.y * self.dim.y

    def draw(self, screen):
        screen.blit(self.image, self.rect)


class Creep(pygame.sprite.Sprite):

    def __init__(self, pos, dim):
        pygame.sprite.Sprite.__init__(self)
        self.image = pygame.Surface([dim.x, dim.y])
        self.image.fill((60, 200, 60))
        self.rect = self.image.get_rect()
        self.rect.x = pos.x * dim.x
        self.rect.y = pos.y * dim.y
        self.dim = dim

    def get_position(self):
        return vec2d((self.rect.x // self.dim.x, self.rect.y // self.dim.y))

    def draw(self, screen):
        screen.blit(self.image, self.rect)


class Wall(pygame.sprite.Sprite):

    def __init__(self, pos, dim):
        pygame.sprite.Sprite.__init__(self)
        self.image = pygame.Surface([dim.x, dim.y])
        self.image.fill((0, 0, 0))
        self.rect = self.image.get_rect()
        self.rect.x = pos.x * dim.x
        self.rect.y = pos.y * dim.y
        self.dim = dim

    def get_position(self):
        return vec2d((self.rect.x // self.dim.x, self.rect.y // self.dim.y))

    def draw(self, screen):
        screen.blit(self.image, self.rect)


class GridWorld(PyGameWrapper):
    """
    Parameters
    ----------
    width : int
        Screen width.

    height : int
        Screen height, recommended to be same dimension as width.

    num_creeps : int (default: 3)
        The number of creeps on the screen at once.
    """

    def __init__(self,
                 zoom=16,
                 grid_width=9,
                 grid_height=9,
                 num_creeps=1):

        actions = {
            "up": K_w,
            "left": K_a,
            "right": K_d,
            "down": K_s
        }

        screen_width = zoom * grid_width
        screen_height = zoom * grid_height

        PyGameWrapper.__init__(self, screen_width, screen_height, actions=actions)
        self.max_ticks = 200
        self.screen_width = screen_width
        self.screen_height = screen_height
        self.grid_width = grid_width
        self.grid_height = grid_height
        self.bg_color = (255, 255, 255)
        self.n_creeps = num_creeps
        self.creep_counts = {
            "GOOD": 0
        }
        self.rewards["tick"] = -1
        self.rewards["win"] = 10
        self.rewards["loss"] = -10
        self.rewards["positive"] = 0
        self.player_vel = vec2d((0, 0))
        self.player = None
        self.player_dim = vec2d((zoom, zoom))
        self.wall_dim = vec2d((zoom, zoom))
        self.creep_dim = vec2d((zoom, zoom))
        self.creeps = None
        self.walls = None
        self.recent_options = []
        self.lives = -1
        self.last_action = 4

    def _handle_player_events(self):
        self.player_vel = vec2d((0, 0))
        self.last_action = 4
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                pygame.quit()
                sys.exit()
            if event.type == pygame.KEYDOWN:
                key = event.key
                if key == self.actions["left"]:
                    self.player_vel.x = -1
                    self.last_action = 1
                if key == self.actions["right"]:
                    self.player_vel.x = 1
                    self.last_action = 2
                if key == self.actions["up"]:
                    self.player_vel.y = -1
                    self.last_action = 0
                if key == self.actions["down"]:
                    self.player_vel.y = 1
                    self.last_action = 3

    def _add_creep(self):
        valid_pos = False
        while not valid_pos:
            creep_pos = vec2d((np.random.randint(0, self.grid_width),
                               np.random.randint(0, self.grid_height)))
            valid_pos = not self.maze[creep_pos.x, creep_pos.y]
            player_tile = self.player.get_position()
            if creep_pos.x == player_tile.x and creep_pos.y == player_tile.y:
                valid_pos = False
        creep = Creep(creep_pos, self.creep_dim)
        self.creeps.add(creep)
        self.creep_counts['GOOD'] += 1

    def getGameState(self):
        state = {
            "maze": self.maze,
            "score": self.score,
            "ticks": self.ticks,
            "recent_options": self.recent_options,
            "is_game_over": self.game_over(),
            "player_tile": [self.player.get_position().x, self.player.get_position().y],
            "creep_pos": [],
            "wall_pos": []
        }
        for creep in self.creeps:
            state["creep_pos"].append([creep.get_position().x, creep.get_position().y])
        for wall in self.walls:
            state["wall_pos"].append([wall.get_position().x, wall.get_position().y])
        return state

    def getScore(self):
        return self.score

    def game_over(self):
        """
            Return bool if the game has 'finished'
        """
        return ((self.creep_counts['GOOD'] == 0) or (self.ticks == self.max_ticks))

    def init(self, state=None):
        """
            Starts/Resets the game to its inital state
        """
        # load state
        if state is not None:
            self.creep_counts = {"GOOD": 0}
            self.maze = state["maze"]
            self.score = state["score"]
            self.ticks = state["ticks"]
            self.recent_options = state["recent_options"]

            # player
            self.player = Player(vec2d((state["player_tile"][0], state["player_tile"][1])), self.player_dim)

            # creep
            if self.creeps is None:
                self.creeps = pygame.sprite.Group()
            else:
                self.creeps.empty()
            creep = Creep(vec2d((state["creep_pos"][0][0], state["creep_pos"][0][1])), self.creep_dim)
            self.creeps.add(creep)
            self.creep_counts['GOOD'] += 1

            # walls
            if self.walls is None:
                self.walls = pygame.sprite.Group()
            else:
                self.walls.empty()
            for i in range(self.grid_width):
                for j in range(self.grid_height):
                    if self.maze[i, j]:
                        wall = Wall(vec2d((i, j)), self.wall_dim)
                        self.walls.add(wall)

        # initialize new state
        else:
            self.creep_counts = {"GOOD": 0}

            # load random maze
            data_dir = "mazes/9x9_sorted/31/"
            files = [f for f in listdir(data_dir) if isfile(join(data_dir, f))]
            maze_id = np.random.randint(len(files))
            self.maze = np.load(data_dir + files[maze_id])

            # player
            valid_pos = False
            while not valid_pos:
                player_tile = vec2d((np.random.randint(0, self.grid_width),
                                    np.random.randint(0, self.grid_height)))
                #player_tile = vec2d((0, 0))

                valid_pos = not self.maze[player_tile.x, player_tile.y]
            self.player = Player(player_tile, self.player_dim)

            # creeps
            if self.creeps is None:
                self.creeps = pygame.sprite.Group()
            else:
                self.creeps.empty()
            for i in range(self.n_creeps):
                self._add_creep()

            # walls
            if self.walls is None:
                self.walls = pygame.sprite.Group()
            else:
                self.walls.empty()
            for i in range(self.grid_width):
                for j in range(self.grid_height):
                    if self.maze[i, j]:
                        wall = Wall(vec2d((i, j)), self.wall_dim)
                        self.walls.add(wall)

            self.score = 0
            self.ticks = 0
            self.recent_options = []

    def step(self, dt):
        """
            Perform one step of game emulation.
        """
        dt /= 1000.0

        self.screen.fill(self.bg_color)
        self._handle_player_events()
        player_tile_old = self.player.get_position()
        player_tile_new = vec2d((player_tile_old.x + self.player_vel.x,
                                 player_tile_old.y + self.player_vel.y))
        self.player.set_position(player_tile_new)

        # handle wall collisions
        wall_hits = pygame.sprite.spritecollide(self.player, self.walls, False)
        oob_x = (player_tile_new.x < 0) or (player_tile_new.x > self.grid_width-1)
        oob_y = (player_tile_new.y < 0) or (player_tile_new.y > self.grid_height-1)
        if len(wall_hits) or oob_x or oob_y:
            self.player.set_position(player_tile_old)

        # handle creep collisions
        creep_hits = pygame.sprite.spritecollide(self.player, self.creeps, False)
        for _ in creep_hits:
            self.creep_counts["GOOD"] -= 1

        self.walls.draw(self.screen)
        self.creeps.draw(self.screen)
        self.player.draw(self.screen)

        player_tile = self.player.get_position()
        self.score += self.rewards["tick"]
        self.recent_options.append(self.last_action)
        self.ticks += 1

        # win
        if self.creep_counts["GOOD"] == 0:
            self.score += self.rewards["win"]

        # lose
        if self.ticks == self.max_ticks:
            self.score += self.rewards["loss"]


if __name__ == "__main__":
    import numpy as np

    pygame.init()
    game = GridWorld(width=256, height=256, num_creeps=10)
    game.screen = pygame.display.set_mode(game.getScreenDims(), 0, 32)
    game.clock = pygame.time.Clock()
    game.rng = np.random.RandomState(24)
    game.init()

    while True:
        dt = game.clock.tick_busy_loop(30)
        game.step(dt)
        pygame.display.update()
