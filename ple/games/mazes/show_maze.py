#!/usr/bin/env python

import argparse
import numpy as np
import matplotlib.pyplot as plt

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("id", type=str)
    args = parser.parse_args()
    m = np.load(args.id + '.npy')
    plt.figure(figsize=(2, 2))
    plt.imshow(np.transpose(m), cmap=plt.cm.binary, interpolation='nearest')
    plt.xticks([]), plt.yticks([])
    plt.show()
