#!/bin/bash

n_mazes=1000
rng_offset=0
n_cpus=10
width=5
height=5
out_path='5x5'

let "r=n_mazes / n_cpus"

for i in `seq 0 $r`; do
    for j in `seq 1 $n_cpus`; do
        let "a=$i * $n_cpus + $j + $rng_offset"
        python gen_maze.py $a $width $height $out_path &
    done
    wait
    echo generated $a mazes
done
