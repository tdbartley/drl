#!/usr/bin/env python

import os
import argparse
import numpy as np
from numpy.random import randint

def maze(seed=0, width=17, height=17, complexity=0.75, density=.75):
    np.random.seed(seed)
    # Only odd shapes
    shape = ((height // 2) * 2 + 3, (width // 2) * 2 + 3)
    # Adjust complexity and density relative to maze size
    complexity = int(complexity * (5 * (shape[0] + shape[1])))
    density    = int(density * ((shape[0] // 2) * (shape[1] // 2)))
    # Build actual maze
    Z = np.zeros(shape, dtype=bool)
    # Fill borders
    Z[0, :] = Z[-1, :] = 1
    Z[:, 0] = Z[:, -1] = 1
    # Make aisles
    for i in range(density):
        x, y = randint(0, shape[1] // 2) * 2, randint(0, shape[0] // 2) * 2
        Z[y, x] = 1
        for j in range(complexity):
            neighbours = []
            if x > 1:             neighbours.append((y, x - 2))
            if x < shape[1] - 2:  neighbours.append((y, x + 2))
            if y > 1:             neighbours.append((y - 2, x))
            if y < shape[0] - 2:  neighbours.append((y + 2, x))
            if len(neighbours):
                y_,x_ = neighbours[randint(0, len(neighbours) - 1)]
                if Z[y_, x_] == 0:
                    Z[y_, x_] = 1
                    Z[y_ + (y - y_) // 2, x_ + (x - x_) // 2] = 1
                    x, y = x_, y_

    return Z[1:shape[0]-1, 1:shape[1]-1]


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("seed", type=int)
    parser.add_argument("width", type=int)
    parser.add_argument("height", type=int)
    parser.add_argument("path", type=str)
    args = parser.parse_args()
    m = maze(args.seed, args.width, args.height)
    wall_count = np.sum(m)
    directory = args.path + '/' + str(wall_count)
    if not os.path.exists(directory):
        os.makedirs(directory)
    np.save(directory + '/' + str(args.seed), m)
