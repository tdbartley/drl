#!/usr/bin/env python
import pickle
import pygame
import numpy as np
from ple.games.waterworldmaze import WaterWorldMaze

if __name__ == "__main__":
    game = WaterWorldMaze()
    game.screen = pygame.display.set_mode(game.getScreenDims())
    game.clock = pygame.time.Clock()
    game.rng = np.random.RandomState(24)
    game.init()

    while True:
        if game.game_over():
            game.reset()
        dt = game.clock.tick_busy_loop(30)
        game.step(dt)
        pygame.display.update()
